# Auto Tab Mute

## About

This is a chrome extension that automatically mutes background tabs if the current tab is playing audio. This allows for fast switching between different audio playing tabs without manually muting and unmuting. This function can be toggled on/off by a button added to the browser interface or by a keyboard shortcut (Alt-Shift-U by default).
